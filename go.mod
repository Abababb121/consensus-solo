module chainmaker.org/chainmaker/consensus-solo/v3

go 1.16

require (
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/consensus-utils/v3 v3.0.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/protocol/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/pingcap/errors v0.11.5-0.20201126102027-b0a155152ca3 // indirect
	github.com/pingcap/log v0.0.0-20201112100606-8f1e84a3abc8 // indirect
	github.com/stretchr/testify v1.8.0
	go.uber.org/zap v1.19.1 // indirect
)
